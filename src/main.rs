use reqrnpdno::{extractora,parameters::Parametros};


fn bajar_diccionarios(){
// definir carpeta de descarga
let output = "./data".to_string();
// descargar diccionarios
extractora::get_diccionarios(&output).unwrap();
}


fn bajar_datos(){
// idem
let output = "./data/raw".to_string();
// primera descarga de datos sin filtros
let mut params = Parametros::new();
// extraer por estados
extractora::extraer_por_estados(&params,&output).unwrap();
}

fn main() {
    println!("===== Obteniendo diccionarios =====");
    bajar_diccionarios();
    println!("===== Descargando datos =====");
    bajar_datos();
}
