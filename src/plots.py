#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# paquetes
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


#functions
# abrir archivos json
def abrir_json(ruta, nombre):
    """
    Esta funcion recibe dos parametros string con la ubicacion del archivo que
    se requiere abrir y el nombre temporal que se le asigna al objeto json.
    """
    with open(ruta) as nombre:
        data=json.loads(nombre.read())
    return data

# parsear datos del json y convertirlos a data frame
def transfomar_jsonadf(datos, seleccion):
    """
    Esta otra funcion recibe el json generado por la funcion abrir_json() 
    como primer parametro, el segundo parametro es un string que sirve para 
    seleccionar un objeto dentro del jsony lo convierte en un conjunto de datos
    de Pandas. Es necesario transponer el conjunto para darle formato long. 
    """
    dataframe=pd.DataFrame.from_dict(data=datos[seleccion], orient='index')
    # transponer datos y reestablecer indice
    dataframe=dataframe.T.reset_index()
    return dataframe


# abrir diccionario de estados
json_dicc_estados = abrir_json('./data/diccionarios/estados.json','dicc')

# primero veamos datos a nivel nacional -0- segun el diccionario
json_estados = abrir_json('./data/raw/estados/0.json','data')


estados_data = transfomar_jsonadf(json_estados,'espacial')


# algunos estadisticos descriptivos
estados_data['Hombre'].sum()
estados_data['Mujer'].sum()
estados_data.describe()
estados_data.sum()
estados_data.shape
estados_data.columns

# generear columna con totales
estados_data['Total'] = estados_data[['Hombre','Mujer','Indeterminado']].sum(axis=1)


# algunas graficas
sns.set()

# totales
estados_data = estados_data.sort_values(by=['Total'])

# plotear
plt.figure(figsize=(12,12))
plt.barh(estados_data['index'],estados_data['Total'])
plt.title('Personas desaparecidas, no localizadas y localizadas reportadas en el RNPEDNO')
plt.xlabel('Estado')
plt.ylabel('Total')
#plt.show()
plt.savefig('./plots/total_reportadas.png', dpi=300, bbox_inches='tight')

# consultar por anios
anual = transfomar_jsonadf(json_estados,'anual')

# generar totales
anual['Total'] = anual[['Hombre','Mujer','Indeterminado']].sum(axis=1)

# quitar registros sin fecha - anio de referencia
anual=anual[1:]

# line plot
plt.figure(figsize=(12,12))
plt.plot(anual['index'],anual['Hombre'], label='Hombres' ,color='blue')
plt.plot(anual['index'],anual['Mujer'], label ='Mujeres', color='green')
plt.plot(anual['index'],anual['Indeterminado'], label ='Indet.', color='orange')
plt.xticks(rotation=90)
plt.title('Personas desaparecidas, no localizadas y localizadas reportadas en el RNPEDNO')
plt.xlabel('Anual')
plt.ylabel('Total')
plt.show()

# guardar
plt.savefig('./plots/total_reportadas_anual.png', dpi=300, bbox_inches='tight')


# histograma por edad
edades = transfomar_jsonadf(json_estados, 'por_edad')

# quitar edades sin referencia
edades=edades[:-1]

# generar totales
edades['Total']=edades[['Hombres','Indeterminado','Mujeres']].sum(axis=1)

# renombrar indice
edades.rename(columns = {'index':'Edad'}, inplace = True)

edades['Edad'] = edades['Edad'].astype('int64')

edades = pd.melt(edades, id_vars='Edad', var_name='Sexo', value_name='Total', ignore_index=True)

#plt.figure(figsize=(12,12))
#plt.boxplot([edades['Hombres'], edades['Mujeres']])
sns.boxplot(data=edades)
plt.show()

